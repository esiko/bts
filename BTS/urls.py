from django.contrib import admin
from django.urls import path
from api.views import *

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/routes/<str:route_number>/', RouteInfoView.as_view()),
    path('routes/', route_number_list),
    path('routes/<str:route_number>/', route_info)
]
