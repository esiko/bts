from rest_framework import serializers
from .models import BusGPSData


class BusGPSDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = BusGPSData
        fields = ['busPlateNumber', 'lat', 'lon', 'routeNumber']
