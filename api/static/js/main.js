document.addEventListener("DOMContentLoaded", function () {
    var map = L.map('map').setView([51.15, 71.45], 11);
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(map);

    var busIcon = L.icon({
        iconUrl: "/static/images/bus.png",
        iconSize: [32, 32],
        iconAnchor: [16, 16],
        popupAnchor: [0, -16]
    });

    var routeNumber = window.location.pathname.split('/').slice(-2, -1)[0];

    fetch(`/api/routes/${routeNumber}/`)
        .then(response => response.json())
        .then(data => {
            data.forEach(point => {
                var marker = L.marker([point.lat, point.lon], { icon: busIcon }).addTo(map);
                marker.bindPopup(point.busPlateNumber);
            });
        });
});
