from django.shortcuts import render
from django.http import JsonResponse
from rest_framework.views import APIView
from .models import BusGPSData
from .serializer import BusGPSDataSerializer


class RouteInfoView(APIView):
    def get(self, request, route_number):
        queryset = BusGPSData.objects.filter(routeNumber=route_number)
        serializer = BusGPSDataSerializer(queryset, many=True)
        data = serializer.data
        return JsonResponse(data, safe=False)


def route_number_list(request):
    route_numbers = BusGPSData.objects.values_list('routeNumber', flat=True).distinct()
    route_numbers = sorted(route_numbers, key=lambda x: int(x))
    return render(request, 'main.html', {'route_numbers': route_numbers})


def route_info(request, route_number):
    route_numbers = BusGPSData.objects.values_list('routeNumber', flat=True).distinct()
    route_numbers = sorted(route_numbers, key=lambda x: int(x))
    return render(request, 'main.html', {'route_numbers': route_numbers})
